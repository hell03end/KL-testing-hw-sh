#!/bin/bash

echo "$(date) - initialazing..." >> $LOG_PATH_TEST
LOCAL_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export TEST_FILE="result.txt"
export GEDIT_PATH="/usr/bin/gedit"
export FIXTURE_STRING_1="Hello, world!"
export FIXTURE_STRING_2="Hello again!"

if [[ -e $TEST_FILE ]]; then
    rm $TEST_FILE
fi

echo "$(date) - run test 'save_text_to_file'" >> $LOG_PATH_TEST
xdotool save_text_to_file >> $LOG_PATH_TEST 2>> $LOG_PATH_TEST
echo "$(date) - wait until '$TEST_FILE' will be created" >> $LOG_PATH_TEST
sleep $AVRG_SLEEP_TIME
perl test_text_in_file.pl FIXTURE_STRING_1 >> $LOG_PATH_TEST 2>> $LOG_PATH_TEST

echo "$(date) - wait until old gedit window will be closed" >> $LOG_PATH_TEST
sleep $MAX_SLEEP_TIME

echo "$(date) - run test 'rewrite_text_to_file'" >> $LOG_PATH_TEST
xdotool rewrite_text_to_file >> $LOG_PATH_TEST 2>> $LOG_PATH_TEST
echo "$(date) - wait until '$TEST_FILE' will be updated" >> $LOG_PATH_TEST
sleep $AVRG_SLEEP_TIME
perl test_text_in_file.pl FIXTURE_STRING_2 >> $LOG_PATH_TEST 2>> $LOG_PATH_TEST

echo "$(date) - wait until '$TEST_FILE' will be released from perl test" >> $LOG_PATH_TEST
sleep $AVRG_SLEEP_TIME

echo "$(date) - remove test file '$TEST_FILE'" >> $LOG_PATH_TEST
rm -f $TEST_FILE
if [[ -e $TEST_FILE ]]; then
    echo "$(date) - WARNING: '$TEST_FILE' wasn't removed!" >> $LOG_PATH_TEST
fi
