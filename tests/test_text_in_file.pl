#!/urs/bin/perl -w

use warnings;
use strict;
use utf8;

my $test_file = $ENV{'TEST_FILE'};
my $fixture_str = $ARGV[0];

if (-f $test_file) {
    open my $fin, $test_file or die "'$0' with '$fixture_str'\tFAILED: Can't open `$test_file`: $!\n";
    my $file_text = <$fin>;  # only first string - that's OK
    close $fin or die "'$0' with '$fixture_str'\tFAILED: Can't close '$test_file': $!\n";

    # smartwatch can be used but now this is experimental feature
    if (grep($fixture_str, $file_text)) {
        print "'$0' with '$fixture_str'\tPASSED\n";
    } else {
        die "'$0' with '$fixture_str'\tFAILED: '$fixture_str' doesn't match '$test_file' content\n";
    }
} else {
    die "'$0' with '$fixture_str'\tFAILED: '$test_file' doesn't exist\n";
}
