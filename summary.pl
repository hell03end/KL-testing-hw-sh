#!/urs/bin/perl -w

use warnings;
use strict;
use utf8;

my $path_to_log = $ENV{'LOG_PATH_TEST'};

if (!-f $path_to_log) {
    exit;
}

open my $fin, $path_to_log or die "Can't open `$path_to_log`: $!\n";
my @file_lines = <$fin>;
close $fin or die "Can't close `$path_to_log`: $!\n";

my $count_failed = 0;
my $count_passed = 0;

foreach my $line (@file_lines) {
    if ($line =~ /PASSED/) {
        $count_passed += 1;
    } elsif ($line =~ /FAILED/) {
        $count_failed += 1;
    }
}

print qq{===== Summary =====
Test PASSED:\t$count_passed
Test FAILED:\t$count_failed\n};
